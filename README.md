# Code4Life (Client Project)
### by Dream Team


## Installing

To get starget, just clone the repo, then

```
npm install
```

And start development server

```
npm start
```

## Deployment

Pushing to master will run deployment on netlify.
You can access the site at

```
https://code4life.netlify.com/
```


## Built With

* [React](https://reactjs.org/)


## Authors

* **kamciokodzi** - *frontend* - [kamciokodzi](https://github.com/kamciokodzi)
* **poe** - *frontend* - [pietersweter](https://github.com/pietersweter)
* **lukaszkobylecki** - *backend* - [lukaszkobylecki](https://github.com/lukaszkobylecki)
* **matkapp** - *backend* - [matkapp](https://github.com/matkapp)


## License

This project is licensed under the MIT License.