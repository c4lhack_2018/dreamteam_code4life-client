import React, { Component } from 'react';
import QuestionList from '../../components/QuestionList/QuestionList';
import PlacesList from '../../components/PlacesList/PlacesList';
import AppLogout from '../../components/AppLogout/AppLogout';
import axios from 'axios';
import './ParentView.scss';
import ParentQuestionDetails from '../../components/ParentQuestionDetails/ParentQuestionDetails';
import AboutUs from '../../components/AboutUs/AboutUs'

const server = require('../../config/server.json');

export default class ParentView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: this.props.questions,
      selectedQuestion: null,
      doctors: [],
      schools: []
    };

    axios.get('https://autismtest.azurewebsites.net/api/Doctor')
        .then(resp => {
          console.log(resp)
          this.setState({
            doctors: resp.data,
            isDataFetched: true
          })
        });
        axios.get('https://autismtest.azurewebsites.net/api/MedicalInstitution')
        .then(resp => {
          console.log(resp)
          this.setState({
            schools: resp.data,
            isDataFetched: true
          })
        });
    this.selectQuestion = this.selectQuestion.bind(this);
    // this.fetchData = this.fetchData.bind(this);
  }
  
  selectQuestion(selectedQuestionFirstQuestionId, selectedQuestionId) {
    axios.get(`${server.url}/Question/${selectedQuestionFirstQuestionId}`)
		.then(resp => resp.data)
		.then(data => {
      // console.log(data);
			this.setState({
        selectedQuestion: data,
        selectedQuestionId: selectedQuestionId 
			}, () => {
        this.props.selectView('question');
      });
    })
  }


  render() {
    let parentViewContent;

    switch(this.props.currentStatus) {
      case 'home':
        parentViewContent = (
          <div className="ParentView__content">
            <QuestionList selectQuestion={this.selectQuestion}/>
          </div>
        );
        break;
      case 'logout':
        parentViewContent = (
          <AppLogout 
            selectUser={this.props.selectUser}
            selectView={this.props.selectView}
          />
        );
        break;
      case 'question':
        parentViewContent = (
          <ParentQuestionDetails
            selectedQuestion={this.state.selectedQuestion}
            selectedQuestionId={this.state.selectedQuestionId}
            selectView={this.props.selectView}
          />
        );
        break;
      case 'school':
        parentViewContent = (
          <PlacesList data={this.state.schools}/>
        );
        break;
      case 'doctor':
        parentViewContent = (
          <PlacesList data={this.state.doctors}/>
        );
        break;
      case 'about':
        parentViewContent = (
        <AboutUs />
        );
      break;
      default:
        break;
    }

    return (
      <div className="ParentView App__animate-fade-slide">
        { parentViewContent }
        <div className="App__curtain App__animate-fade-slide">
          {/* <div className="LandingView__curtain-top"></div> */}
          {/* <div className="LandingView__curtain-bottom"></div> */}
        </div>
        <div className="LandingView__curtain">
          <div className="LandingView__curtain-top"></div>
          <div className="LandingView__curtain-bottom"></div>
        </div>
      </div>
    )
  }
}