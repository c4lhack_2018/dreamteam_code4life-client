import React, { Component } from 'react';
import QuestionList from '../../components/QuestionList/QuestionList';
import PlacesList from '../../components/PlacesList/PlacesList';
import AppLogout from '../../components/AppLogout/AppLogout';
import axios from 'axios';
import './DoctorView.scss';
import DoctorDataList from '../../components/DoctorDataList/DoctorDataList';
import DoctorDataItem from '../../components/DoctorDataItem/DoctorDataItem';
// import DoctorQuestionDetails from '../../components/DoctorQuestionDetails/DoctorQuestionDetails';

const server = require('../../config/server.json');

export default class DoctorView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: this.props.questions,
      selectedQuestion: null
    };

    this.selectQuestion = this.selectQuestion.bind(this);
  }
  selectQuestion(selectedQuestionFirstQuestionId, selectedQuestionId) {
    axios.get(`${server.url}/Question/${selectedQuestionFirstQuestionId}`)
		.then(resp => resp.data)
		.then(data => {
      // console.log(data);
			this.setState({
        selectedQuestion: data,
        selectedQuestionId: selectedQuestionId 
			}, () => {
        this.props.selectView('question');
      });
    })
  }

  render() {
    let doctorViewContent;
    console.log(this.props.currentStatus);
    switch(this.props.currentStatus) {
      case 'about':
        doctorViewContent = (
          <div className="App__content App__content-big">o nas</div>
        );
        break;
      case 'home':
        doctorViewContent = (
          <div className="DoctorView__content App__animate-fade-slide">
            {this.props.doctorDataList.map(data => <DoctorDataItem 
            />)}
          </div>
        );
        break;
      case 'logout':
        doctorViewContent = (
          <AppLogout 
            selectUser={this.props.selectUser}
            selectView={this.props.selectView}
          />
        );
        break;
      default:
        break;
    }

    return (
      <div className="DoctorView App__animate-fade-slide">
        { doctorViewContent }
        <div className="App__curtain App__animate-fade-slide">
          {/* <div className="LandingView__curtain-top"></div> */}
          {/* <div className="LandingView__curtain-bottom"></div> */}
        </div>
        <div className="LandingView__curtain">
          <div className="LandingView__curtain-top"></div>
          <div className="LandingView__curtain-bottom"></div>
        </div>
      </div>
    )
  }
}