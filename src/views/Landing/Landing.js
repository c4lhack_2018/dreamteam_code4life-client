import React, { Component } from 'react'

import AppButton from '../../components/AppButton/AppButton';

import AppLoginForm from '../../components/AppLoginForm/AppLoginForm';
import AppRegisterForm from '../../components/AppRegisterForm/AppRegisterForm';
import AboutUs from '../../components/AboutUs/AboutUs'

import './Landing.scss';

export default class Landing extends Component {
  render() {
    let landingViewContent;

    switch(this.props.currentStatus) {
      case 'home':
        landingViewContent = (
          <div className="LandingView__content">
            <div className="App__content App__content-big">
              Witaj w <br/>  ASign
            </div>
          </div>
        );
        break;
      case 'register':
        landingViewContent = (
          <div className="LandingView__content">
            <AppRegisterForm selectView={this.props.selectView} />
          </div>
        );
        break;
      case 'login':
        landingViewContent = (
          <div className="LandingView__content">
            <AppLoginForm 
              fillQuestions={this.props.fillQuestions}
              fillEmails={this.props.fillEmails}
              selectUser={this.props.selectUser}
              selectView={this.props.selectView}
            />
          </div>
        );
        break;
      case 'about':
        landingViewContent = (
        <AboutUs />
        );
      break;
      default:
        break;
    }

    return (
      <div className="LandingView">
        { landingViewContent }
      </div>
    )
  }
}
