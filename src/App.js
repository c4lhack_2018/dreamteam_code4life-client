import React, { Component } from 'react'

import AppHeader from './components/AppHeader/AppHeader';
import ParentView from './views/ParentView/ParentView';
import Landing from './views/Landing/Landing';

import AppRegisterForm from './components/AppRegisterForm/AppRegisterForm';

import 'normalize.css';
import './App.scss';
import DoctorView from './views/DoctorView/DoctorView';

export default class App extends Component {
  constructor() {
    super();

    this.state = {
      currentUser: 'notLoggedIn',
      currentStatus: 'home',
      questions: [],
      emails: []
    }

    this.selectUser = this.selectUser.bind(this);
    this.selectView = this.selectView.bind(this);
    this.fillQuestions = this.fillQuestions.bind(this);
    this.fillEmails = this.fillEmails.bind(this);
  }

  selectView(selectedView) {
    console.log(selectedView);
    this.setState({
      currentStatus: selectedView
    });
  }

  selectUser(selectedUser) {
    this.setState({
      currentUser: selectedUser
    }, ()=>{
      console.log('nowy user to ' + this.state.currentUser);
    });
  }

  fillQuestions(questions) {
    this.setState({
      questions: questions
    }, () => {
      console.log(this.state.questions);
    });
  }

  fillEmails(emails) {
    console.log('asd');
    console.log(emails);
    this.setState({
      emails: emails,
      currentUser: 'doctor',
      currentStatus: 'home'
    }, () => {
      // console.log(this.state.questions);
    });
  }

  render() {
    let view;
    switch (this.state.currentUser) {
      case 'parent':
        view = <ParentView
          currentStatus={this.state.currentStatus} 
          selectUser={this.selectUser}
          selectView={this.selectView}
          questions={this.state.questions} />
        break;
      case 'notLoggedIn':
        view = <Landing 
          fillQuestions={this.fillQuestions}
          fillEmails={this.fillEmails}
          currentStatus={this.state.currentStatus}
          selectView={ this.selectView }
          selectUser={this.selectUser} />
        break;
      case 'register':
        view = <AppRegisterForm  />
        break;
      case 'doctor':
        view = <div className="DoctorDataList">
          {
            this.state.emails.map(email => <div className="DoctorDataItem">
              <div className="DoctorDataItem__email">{email.email}</div>
              <div className="DoctorDataItem__bottom">
                <div className="DoctorDataItem__text">
                  {email.testId}
                </div>
              </div>
            </div>)
          }
        </div>
      default:
        break;
    }
    return (
      <div className="App">
        <AppHeader 
          selectView={ this.selectView }
          currentUser={ this.state.currentUser }
          currentStatus = { this.state.currentStatus }
        />
        { view }
        <footer className="LandingView__curtain">
          <div className="LandingView__curtain-top"></div>
          <div className="LandingView__curtain-bottom"></div>
        </footer>
      </div>
    )
  }
}
