const filters = {
  // SHOW_ALL: 'show all',
  FINISHED: 'finished',
  IN_PROGRESS: 'in progress',
  NOT_STARTED: 'not started'
}

export default filters;