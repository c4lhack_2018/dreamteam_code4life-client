const questionsStatusToColor = {
  SHOW_ALL: 'red',
  FINISHED: 'blue',
  IN_PROGRESS: 'yellow',
  NOT_STARTED: 'orange'
}

export default questionsStatusToColor;