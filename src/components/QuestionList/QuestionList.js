import React, { Component } from 'react'
import QuestionItem from "../QuestionItem/QuestionItem"
import './QuestionList.scss';
import axios from 'axios';

import AppCircleSpinner from '../AppCircleSpinner/AppCircleSpinner';
import AppSearchContainer from '../AppSearchContainer/AppSearchContainer';

const server = require('../../config/server.json');

// {
// 	body: "This is Third question",
// 	status: "Not started",
// 	url: "https://cdn.weasyl.com/static/media/5f/ee/6f/5fee6f978532c33bafb9c745b042119b28abc468db112e6437569d75a11ce72b.jpg"
// }

export default class QuestionList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			questions: [],
			isDataFetched: false,
			searchUsed: false							// only for the first time when getting data, we want to add delay to our animation (the whole list is generated statically)
		};

		// Just to show off our fancy spinner :)
		setTimeout(()=>{
			this.fetchData();
		}, 1500);
	

		this.selectQuestion = this.selectQuestion.bind(this)
		this.updateQuestions = this.updateQuestions.bind(this);
		this.fetchData = this.fetchData.bind(this);
	}

	selectQuestion(selectedQuestion) {
	
	}

	_mock_getRandomStatus() {
		const tmpRand = Math.random();
		const possibleStatuses = ['not started', 'in progress', 'finished'];
		let randomStatus = possibleStatuses[0];
		const interval = 1/possibleStatuses.length;
		for (let i=0; i<=possibleStatuses.length; i++) {
			if (tmpRand >= i*interval) {
				randomStatus = possibleStatuses[i];
			}
		}
		return randomStatus;
	}

	fetchData() {
		axios.get(`${server.url}/Exercise`)
			.then(resp => resp.data)
			.then(data => {
				console.log(data);
				data.forEach(question => {
					question.status = this._mock_getRandomStatus();	
				});
				return data;
			})
			.then(data => {
				this.setState({
					questions: data,
					isDataFetched: true
				});
			})
			.catch(err => {
				console.log(err)
			});	
	}

	updateQuestions(questions) {
		this.setState({
			questions: questions,
			isDataFetched: true,
			searchUsed: true
		});
	}

render() {
		return (
			this.state.isDataFetched ?
			<div>
				<AppSearchContainer 
					updateQuestions={this.updateQuestions} 
					questions={this.state.questions} 
				/>
				<div className={`QuestionList App__animate-fade-slide ${this.state.searchUsed ? 'search-used' : ''}`}>
					{
						this.state.questions.sort((a,b) => {
								if(a.status < b.status) return 1;
								if(a.status > b.status) return -1;
								return 0;
							}).map((question, index) => 
								<QuestionItem 
									question={question}
									key={index}
									handleClick={this.props.selectQuestion} 
									id={question.id}
									firstQuestionId={question.firstQuestionId}
								/>
						)
					}
				</div>
			</div> :
			<AppCircleSpinner />
		)
	}
}
