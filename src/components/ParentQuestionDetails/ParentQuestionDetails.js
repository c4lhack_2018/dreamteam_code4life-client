import React, { Component } from 'react';
import './ParentQuestionDetails.scss';
import AppButton from '../AppButton/AppButton';
import axios from 'axios';

const server = require('../../config/server.json');
export default class ParentQuestionDetails extends Component {
  constructor(props) {
    super(props);

    console.log(this.props);

    this.state = {
      selectedQuestion: this.props.selectedQuestion,
      selectedQuestionId: this.props.selectedQuestionId
    };

    this.validateSubquestion = this.validateSubquestion.bind(this);
  }

  validateSubquestion() {
    const answers = Array.from(document.querySelectorAll('input:checked'));
    const numRadioButtons = this.state.selectedQuestion.subquestions.length;
    console.log(`You've answered ${answers.length} out of ${numRadioButtons} answers`);
    let answer_YES = 0;
    if (answers.length === numRadioButtons) {
      answers.forEach(answer => {
        if (answer.value === 'yes') {
          answer_YES++;
        }
      });
    }
    
    let newQuestion = this.state.selectedQuestion;  
    console.log(`You've passed ${answer_YES} questions.`);

    if (answer_YES >= this.state.selectedQuestion.threshold) {
      console.log(this.state.selectedQuestion);

      if (this.state.selectedQuestion.thresholdPassQuestion) {
        newQuestion = this.state.selectedQuestion.thresholdPassQuestion;

        this.setState({
          selectedQuestion: newQuestion
        });

      } else {
        axios.post(`${server.url}/TestExercise/TestExerciseData`, {
          testId: localStorage.getItem('testId'),
          exerciseId: this.state.selectedQuestionId,
          result: newQuestion.thresholdPassQuestionId
        }).then(resp => {
          console.log(resp.status);
          if (resp.status === 200) {
            this.props.selectView('home');
          }
        }).catch(err => {

        })
      };
    } else if (this.state.secondaryThreshold > 0 && answer_YES >= this.state.secondaryThreshold) {
      if (this.state.selectedQuestion.secondaryThresholdPassQuestion) {
        newQuestion = this.state.selectedQuestion.secondaryThresholdPassQuestion;
        
        this.setState({
          selectedQuestion: newQuestion
        });
      } else {
        axios.post(`${server.url}/TestExercise/TestExerciseData`, {
          testId: localStorage.getItem('testId'),
          exerciseId: this.state.selectedQuestionId,
          result: newQuestion.secondaryThresholdPassQuestionId
        }).then(resp => {
          console.log(resp.status);
          if (resp.status === 200) {
            this.props.selectView('home');
          }
        }).catch(err => {

        })
      };
    } else {
      if (this.state.selectedQuestion.thresholdFailQuestion) {
        newQuestion = this.state.selectedQuestion.thresholdFailQuestion;

        this.setState({
          selectedQuestion: newQuestion
        });
      } else {
        axios.post(`${server.url}/TestExercise/TestExerciseData`, {
          testId: localStorage.getItem('testId'),
          exerciseId: this.state.selectedQuestionId,
          result: newQuestion.thresholdFailQuestionId
        }).then(resp => {
          console.log(resp.status);
          if (resp.status === 200) {
            this.props.selectView('home');
          }
        }).catch(err => {

        })
        
      }
    }
  }

  render() {
    const subquestions = this.state.selectedQuestion.subquestions;
    console.log(subquestions);
    return (
      <div className="ParentQuestionDetails App__animate-fade-slide">
        <div className="ParentQuestionDetails__wrapper">
          <div className="ParentQuestionDetails__concern">{this.state.selectedQuestion.body}</div>
          <div className="ParentQuestionDetails__columns">
            <video
                autoPlay="autoplay"
                type="video/mp4"
                src="/question17.mp4"
                width="600"
                height="500">
              Sorry, your browser doesn't support embedded videos.
            </video>
            <form className="ParentQuestionDetails__form App__animate-fade-slide">
              {
                subquestions.map((sub,index) => <div
                key={index}  
                className="ParentQuestionDetails__subquestion App__animate-fade-slide"  
                >
                  <div className="ParentQuestionDetails__subquestion-text">{sub.body}</div>
                  <div className="ParentQuestionDetails__subquestion-actions">
                    <label><input type="radio" name={`radio-${index}`} value="yes" /><span>Tak</span></label>
                    <label><input type="radio" name={`radio-${index}`} value="no" /><span>Nie</span></label>
                  </div>
                </div>)
              }
              <div className="ParentQuestionDetails__subquestion-actions">
                <button
                  type="button"
                  onClick={this.validateSubquestion}
                  className="AppButton AppButton-secondary"
                >Dalej
                </button>
              </div>
            </form>
            
          </div>
        </div>
      </div>
    )
  }
}
