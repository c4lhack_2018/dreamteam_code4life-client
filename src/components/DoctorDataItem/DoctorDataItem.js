import React, { Component } from 'react'

export default class DoctorDataItem extends Component {
  render(props) {
    console.log(props);
    return (
      <div className="DoctorDataItem">
        <div className="DoctorDataItem__top">
          <div className="DoctorDataItem__email">props.doctorDataItem.email</div>
        </div>
        <div className="DoctorDataItem__bottom">
          <div className="DoctorDataItem__testId">props.doctorDataItem.testId</div>
          <div className="DoctorDataItem__score">props.doctorDataItem.score/props.doctorDataItem.exerciseCount</div>
        </div>
      </div>
    )
  }
}
