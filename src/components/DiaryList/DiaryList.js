import React, { Component } from 'react'
import DiaryItem from '../DiaryItem/DiaryItem';
import './DiaryList.scss'
import axios from 'axios'

export default class DiaryList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      observations: [],
      isDataFetched: false
    }
  };

  selectObservation(selectedObservation) {

  }
  
  render () {
    return (
      <div className="DiaryList">
        {
          this.state.observations.map((observation, index) => 
            <DiaryItem observation={observation} key={index} handleClick={this.selectObservation} />
          )
        }
      </div>
    )
  }
}
