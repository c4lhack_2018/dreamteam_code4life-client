import React, { Component } from 'react';
import AppSearchFilter from '../AppSearchFilter/AppSearchFilter';
import AppSearchBox from '../AppSearchBox/AppSearchBox';

import './AppSearchContainer.scss';
import filters from '../../config/filters';

class Search extends Component {
  constructor(props) {
    super(props);
    this.allQuestions = this.props.questions;

    this.state = {
      questions: this.allQuestions
    };

    this.criteria = {
      searchQuery: '',
      sort: {
        key: 'id',
        direction: 'ascending'
      },
      filter: {
        status: Object.values(filters)
      }
    };

    this.handleSorting = this.handleSorting.bind(this);
    this.handleSearchQuery = this.handleSearchQuery.bind(this);
    this.handleFilterTypes = this.handleFilterTypes.bind(this);
    this.handleFilterCollected = this.handleFilterCollected.bind(this);
    this.sort = this.sort.bind(this);
    this.processSearchQuery = this.processSearchQuery.bind(this);
    this.applyFilters = this.applyFilters.bind(this);
    this.updateResults = this.updateResults.bind(this);
  }

  handleSearchQuery(query) {
    this.criteria.searchQuery = query;
    this.updateResults();
  }

  handleFilterTypes(status) {
    this.criteria.filter.status = status;
    this.updateResults();
  }

  handleFilterCollected(filter) {
    this.criteria.filter.collected = filter;
    this.updateResults();
  }

  handleSorting(key, direction) {
    this.criteria.sort.key = key;
    this.criteria.sort.direction = direction;
    this.updateResults();
  }

  sort(arr) {
    let multiplier = 1;
    const key = this.criteria.sort.key;

    switch(this.criteria.sort.direction) {
      case 'ascending': multiplier = 1; break;
      case 'descending': multiplier = -1; break;
      default: break;
    }

    return arr.sort((a, b) => {
      if (a[key] < b[key]) return -1 * multiplier;
      if (a[key] === b[key]) return 0;
      if (a[key] > b[key]) return multiplier;

      return 0;
    });
  }

  processSearchQuery(arr) {
    const template = this.criteria.searchQuery.toLowerCase();
    console.log(`template -> ${template}`);
    const fields = ['description'];     /// TODO add tags

    return arr.filter(question => {
      for (let field of fields)
        if (question[field].toString().toLowerCase().includes(template)) {
          return true;
        }

      return false;
    });
  }

  applyFilters(arr) {
    return arr.filter(question => this.criteria.filter.status.includes(question.status));
  }

  updateResults() {
    let result = this.applyFilters(this.allQuestions);
        result = this.processSearchQuery(result);
        result = this.sort(result);

    this.setState({
      questions: result
    });

    this.props.updateQuestions(result);
  }

  render() {
    return (
      <div className="AppSearchContainer">
        <AppSearchBox onChange={this.handleSearchQuery} />
        <AppSearchFilter onChange={this.handleFilterTypes} />
      </div>

    );
  }
}

export default Search;