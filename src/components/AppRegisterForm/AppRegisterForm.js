import React, { Component } from 'react'
import './AppRegisterForm.scss'
import axios from 'axios'

export default class AppRegisterForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: {
        name: '',
        surname: '',
        password: '',
        email: '',
        birthDate: null,
        gender: null
      }
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.submit = this.submit.bind(this);
  }

  handleInputChange(event) {
    const newUserData = this.state.userData;
    newUserData[event.target.name] = event.target.value;
    this.setState({
      userData: newUserData
    });
  }

  submit(e) {
    e.preventDefault();
    if(this.state.userData.name && this.state.userData.email && this.state.userData.password) {
      axios.post('https://autismtest.azurewebsites.net/api/Account/Register', {
        name: this.state.userData.name,
        email: this.state.userData.email,
        password: this.state.userData.password
      }).then(resp => {
        console.log(resp)
        localStorage.setItem('email', this.state.userData.email)
        this.props.selectView('landing')
      }).catch(err => {
        console.log(err)
      })
    } else { 
      alert('Proszę uzupełnić wszystkie pola')
    }
  }

  render() {
    return (
      <div className="AppRegisterForm">
        
        <div className="AppRegisterForm__columns">
          <div className="AppRegisterForm__column">
          <video
            className="AppRegisterForm__video"
            autoPlay="autoplay"
            type="video/mp4"
            src="/login.mp4"
            width="600"
            height="500">
            Sorry, your browser doesn't support embedded videos.
          </video>
          </div>
          <div className="AppRegisterForm__column">
            <form className="AppRegisterForm__content">
              <div className="AppRegisterForm__text"> Email: </div>
              <input className="AppRegisterForm__input" type="text" name="email" value={this.state.userData.email} onChange={this.handleInputChange}/>
              <div className="AppRegisterForm__text"> Hasło: </div>
              <input className="AppRegisterForm__input" type="password" name="password" value={this.state.userData.password} onChange={this.handleInputChange}/>
              <button className="AppRegisterForm__button" onClick={this.submit}> Zarejestruj </button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
