import React, { Component } from 'react'
import axios from 'axios'
import PlaceDetail from '../PlaceDetail/PlaceDetail'
import './PlacesList.scss'

export default class PlacesList extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   data: [],
    //   isDataFetched: false
    // };
    // this.fetchData();
  // }

  // () {
    // this.fetchData();
  }

  // fetchData() {
  //   if(this.props.type === 'doctor'){
  //     axios.get('https://autismtest.azurewebsites.net/api/Doctor')
  //       .then(resp => {
  //         console.log(resp)
  //         this.setState({
  //           data: resp.data,
  //           isDataFetched: true
  //         })
  //       })
  //   } else {
  //     axios.get('https://autismtest.azurewebsites.net/api/MedicalInstitution')
  //       .then(resp => {
  //         console.log(resp)
  //         this.setState({
  //           data: resp.data,
  //           isDataFetched: true
  //         })
  //       })
  //   }
  // };

  render () { 
    // console.log(this.state.data)
    return (
      <div className="PlacesList">
        {
          // this.state.isDataFetched &&
          this.props.data.map((place,index) => 
            <PlaceDetail place={place} key={index} handleClick={this.selectPlace} type={this.props.type}/>
          ) 
        }
      </div>
    )
  }
}
