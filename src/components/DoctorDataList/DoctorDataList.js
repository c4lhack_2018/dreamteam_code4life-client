import React, { Component } from 'react'
import DoctorDataItem from '../DoctorDataItem/DoctorDataItem';

export default class DoctorDataList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    console.log('~~~');
    console.log(this.props);
    console.log('~~~');
    return (
      <div className="DoctorDataList">
      {
        this.props.doctorDataList &&
        this.props.doctorDataList.map(doctorDataItem =>
          <DoctorDataItem 
            doctorDataItem={doctorDataItem}
          />
        )
      }
      </div>
    )
  }
}
