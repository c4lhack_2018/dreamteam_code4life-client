import React, { Component } from 'react';
import axios from 'axios';

import './AppLogout.scss';

const server = require('../../config/server.json');
export default class AppLogout extends Component {
  constructor(props) {
    super(props);
    
    this.TIME_BEFORE_LOGOUT = 1200;
  }

  componentDidMount() {
    axios.post(`${server.url}/Account/Logout`)
    .then(resp => {
      console.log(resp);
    });

    setTimeout(()=>{
      console.log('zostales wylogowany');
      this.props.selectUser('notLoggedIn');
      this.props.selectView('home');
    }, this.TIME_BEFORE_LOGOUT);
  }

  render() {
    return (
      <div className="AppLogout">

      </div>
    )
  }
}
