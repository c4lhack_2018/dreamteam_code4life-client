import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.js';
import AboutUs from './components/AboutUs/AboutUs';

ReactDOM.render(<App />, document.getElementById('root'));
